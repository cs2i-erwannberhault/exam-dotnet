﻿using System.Drawing;

namespace Contact.Models
{
    /// <summary>
    /// Contact
    /// </summary>
    public class Contact
    {
        public string Company { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Image Photo { get; set; }

        public Contact(string company, string firstName, string lastName, Image photo)
        {
            Company = company;
            FirstName = firstName;
            LastName = lastName;
            Photo = photo;
        }

    }
}
