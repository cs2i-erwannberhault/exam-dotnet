﻿using Contact.Controls;
using ExamenDotNet.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Contact.Forms
{
    public partial class Form1 : Form
    {
        List<Models.Contact> contacts = new List<Models.Contact>();

        public Form1()
        {
            InitializeComponent();

            contacts.Add(new Models.Contact("Macrosoft", "Ade", "Coats", Resources.ade));
            contacts.Add(new Models.Contact("Appel", "Chris", "Hoge", Resources.chris));
            contacts.Add(new Models.Contact("Macrosoft", "Jenny", "Miller", Resources.jenny));
            contacts.Add(new Models.Contact("Googel", "Justen", "Parker", Resources.justen));
            contacts.Add(new Models.Contact("Twittr", "Nan", "Harrington", Resources.nan));
            contacts.Add(new Models.Contact("Arocle", "Stevie", "McLean", Resources.stevie));
            contacts.Add(new Models.Contact("Twittr", "Veronika", "Medeiros", Resources.veronika));
        }

        private void cards_Click(object sender, EventArgs e)
        {
            foreach (Models.Contact c in contacts)
            {
                CardContactInfo.createContact(c);
            }
        }
    }
}
